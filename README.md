
## COMO USAR: 

* O sistema carrega todos os produtos logo ao entrar na pagina principal
* Para cadastrar um novo produto, tipo ou imposto, basta acessar seu respectivo botão que está habilitado em todas as paginas.
* Antes de cadastrar um produto ou imposto é necessario que exista ao menos um tipo.
* Todos os impostos de um tipo de produto irão ser somados no calculo de valor da pagina principal.
*       exemplo: Temos 3 impostos de 10,13 e 15% cadastrados no tipo tipo_exemplo_1, no imposto cobrado será da soma desses 3 valores, ou seja (38)
