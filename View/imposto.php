<?php
include '../src/conexao.php';
$nome = isset($_POST['nome']) ? $_POST['nome'] : "";
$valor = isset($_POST['valor']) ? $_POST['valor'] : "";
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : "";

// verifica se os dados foram enviados para salvar
if($nome != "" && $valor != "" && $tipo != ""){

    // verifica se os dados foram salvos com sucesso e avisa ao usuario
    $result = $con->prepare ("insert into imposto(nome,valor,tipo_produto_id) values(?,?,?)");
    if( $result->execute([$nome,$valor,$tipo]))
        echo "<script>alert('Imposto salvo com sucesso!')</script>";
    else
        echo "<script>alert('Falha ao salvar Imposto!')</script>";
}
// busca os tipos produtos para inserir no select
$query2 = "select * from tipo_produto";
$result2 = $con->prepare ($query2);
$result2->execute();
$tipos = $result2->fetchAll(PDO::FETCH_NUM);

// busca os impostos para preencher a tabela
$query3 = "select i.*,tp.nome from imposto i left join tipo_produto tp on tp.id = i.tipo_produto_id";
$result3 = $con->prepare ($query3);
$result3->execute();
$impostos = $result3->fetchAll(PDO::FETCH_NUM);

?>
<html>
<br>
<center>
CADASTRO DE IMPOSTO
</center>
<br>
<br>

<button onclick="cadastro('produto')"> Cadastrar Produto</button>
<button onclick="cadastro('tipo')"> Cadastrar Tipo Produto</button>
<button onclick="cadastro('voltar')"> Voltar</button>
<br>
<br>
<form action="imposto.php" method="post">
    <table>
        <tr>
            <th>Nome:</th>
            <td><input type="text" name="nome"></td>
        </tr>
        <tr>
            <th>Valor:</th>
            <td><input type="text" name="valor"></td>
        </tr>
        <tr>
            <th>Tipo:</th>
            <td>
                <select name="tipo">
                    <?php
                    // carrega select com tipos de produtos
                    foreach ($tipos as $tipo) {
                        ?>
                        <option value="<?= $tipo[0] ?>"><?= $tipo[1] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
    </table>
    <br>
    <button type="submit"> Salvar</button>
</form>
<table border="1">
    <tr>
        <th>
            Nome
        </th>
        <th>
            Valor
        </th>
        <th>
            Tipo
        </th>
    </tr>
    <?php
    // distribui os dados de impostos ja salvos na tabela
    foreach ($impostos as $imposto ) { ?>
        <tr>
            <td>
                <?= $imposto[1]; ?>
            </td>
            <td>
                <?= $imposto[2]; ?>
            </td>
            <td>
                <?= $imposto[4]; ?>
            </td>
        </tr>
    <?php } ?>
</table>
</html>



<?php
include '../src/funcoes.php';
?>
