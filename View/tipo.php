<?php
    include '../src/conexao.php';
    $nome = isset($_POST['nome']) ? $_POST['nome'] : "";
// verifica se os dados foram enviados para salvar
    if($nome != ""){

        // verifica se os dados foram salvos com sucesso e avisa ao usuario
        $result = $con->prepare ("insert into tipo_produto(nome) values(?)");
        if( $result->execute([$nome]))
            echo "<script>alert('Tipo Produto salvo com sucesso!')</script>";
        else
            echo "<script>alert('Falha ao salvar Tipo Produto!')</script>";
    }
// busca os tipos dos produtos para preencher a tabela
    $query = "select * from tipo_produto";
    $result = $con->prepare ($query);
    $result->execute();
    $tipos = $result->fetchAll(PDO::FETCH_NUM);
?>
<html>
    <br>
    <center>
        CADASTRO DE TIPO DO PRODUTO
    </center>
    <br>
    <br>

    <button onclick="cadastro('produto')"> Cadastrar Produto</button>
    <button onclick="cadastro('imposto')"> Cadastrar Imposto</button>
    <button onclick="cadastro('voltar')"> Voltar</button>

    <br>
    <br>
    <form action="tipo.php" method="post">
        <table>
            <tr>
                <th>Nome:</th>
                <td><input type="text" name="nome"></td>
            </tr>
        </table>
        <br>
        <button type="submit"> Salvar</button>
    </form>
    <table border="1">
        <tr>
            <th>
                Nome
            </th>
        </tr>
        <?php
        // distribui os dados  de tipo produto na tabela
        foreach ($tipos as $tipo ) { ?>
        <tr>
            <td>
                <?= $tipo[1]; ?>
            </td>
        </tr>
        <?php } ?>
    </table>
</html>


<?php
include '../src/funcoes.php';
?>
