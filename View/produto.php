<?php
include '../src/conexao.php';
$nome = isset($_POST['nome']) ? $_POST['nome'] : "";
$valor = isset($_POST['valor']) ? $_POST['valor'] : "";
$quantidade = isset($_POST['quantidade']) ? $_POST['quantidade'] : "";
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : "";
// verifica se os dados foram enviados para salvar
if($nome != "" && $valor != "" && $quantidade != "" && $tipo != ""){
    // verifica se os dados foram salvos com sucesso e avisa ao usuario
    $result = $con->prepare ("insert into produto(nome,valor,quantidade,tipo_produto_id) values(?,?,?,?)");
    if( $result->execute([$nome,$valor,$quantidade,$tipo]))
        echo "<script>alert('Produto salvo com sucesso!')</script>";
    else
        echo "<script>alert('Falha ao salvar produto!')</script>";
}
// busca os tipos produtos para inserir no select
$query2 = "select * from tipo_produto";
$result2 = $con->prepare ($query2);
$result2->execute();
$tipos = $result2->fetchAll(PDO::FETCH_NUM);
?>
<html>

<br>
<center>
CADASTRO DE PRODUTO
</center>

<br>
<br>
<button onclick="cadastro('tipo')"> Cadastrar Tipo Produto</button>
<button onclick="cadastro('imposto')"> Cadastrar Imposto</button>
<button onclick="cadastro('voltar')"> voltar</button>

<br>
<br>
<form action="produto.php" method="post">
    <table>
        <tr>
            <th>Nome:</th>
            <td><input type="text" name="nome"></td>
        </tr>
        <tr>
            <th>Valor:</th>
            <td><input type="text" name="valor"></td>
        </tr>
        <tr>
            <th>Quantidade:</th>
            <td><input type="text" name="quantidade"></td>
        </tr>
        <tr>
            <th>Tipo:</th>
            <td>
                <select name="tipo">
                    <?php
                    // distribui os dados  de tipo produto na tabela
                    foreach ($tipos as $tipo) {
                        ?>
                        <option value="<?= $tipo[0] ?>"><?= $tipo[1] ?></option>
                        <?php
                        }
                    ?>
                </select>
            </td>
        </tr>
    </table>
    <br>
    <button type="submit"> Salvar</button>
</form>


</html>


<?php
include '../src/funcoes.php';
?>
