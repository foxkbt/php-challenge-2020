<?php
include 'src/conexao.php';
// procura produtos no banco de dados
$query = "select * from produto ";
$result = $con->prepare ($query);
$result->execute();
// contem dados dos produtos retornados
$produtos = $result->fetchAll(PDO::FETCH_NUM);
?>

<html>

<br>
<center>VENDA DE PRODUTOS</center>
<br>
<br>
<button onclick="cadastro('produto')"> Cadastrar Produto</button>
<button onclick="cadastro('tipo')"> Cadastrar Tipo Produto</button>
<button onclick="cadastro('imposto')"> Cadastrar Imposto</button>
<br>
<br>
<table border="1">
    <tr>
        <th>
            Nome
        </th>
        <th>
            Quantidade
        </th>
        <th>
            Valor
        </th>
        <th>
            Valor Total
        </th>
        <th>
            Imposto
        </th>
    </tr>
        <?php
        // distribui os dados na tabela
        foreach ($produtos as $produto) {

                // procura impostos no banco de dados para cada produto pelo seu tipo
                $query2 = "select * from imposto where tipo_produto_id = ?";
                $result2 = $con->prepare ($query2);
                $tipo = $produto[4];
                $result2->execute([$tipo]);
                // carrega dados dos impostos do produto atual do loop
                $impostos = $result2->fetchAll(PDO::FETCH_NUM);

                // soma a % de todos impostos daquele tipo de produto
                $imposto_final = 0;
                foreach ($impostos as $imposto) {
                    $imposto_final += $imposto[2];
                }
                // soma do valor total do produto valor * quantidade
                $valor_total = $produto[2] * $produto[3];
                // calcula a porcentagem do valor do produto
                $valor_final = ((1-($imposto_final/100))*$produto[2]) * $produto[3];
                // reduz a variavel a 2 decimais
                $valor_final = number_format($valor_final, 2, '.', '');
            ?>
        <tr>
            <td>
                <?= $produto[1];  // nome ?>
            </td>
            <td>
                <?= $produto[3];  // quantidade ?>
            </td>
            <td>
                <?= $produto[2];  // valor ?>
            </td>
            <td>
                <?="R$:".$valor_total ; // valor total ?>
            </td>
            <td>
                <?= "R$:".($valor_total-$valor_final) ; // imposto em % ?>
            </td>
        </tr>
        <?php } ?>
</table>
</html>
<?php
// carrega funções java script
 include 'src/funcoes.php';
?>