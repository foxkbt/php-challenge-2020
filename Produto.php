<?php


class Produto
{
    private $id;
    private $nome;
    private $quantidade;
    private $valor;

    public function __construct($id,$nome,$quantidade,$valor)
    {
        $this->id = $id;
        $this->nome = $nome;
        $this->quantidade = $quantidade;
        $this->valor = $valor;
    }

    function setId($id){
        $this->id = $id;
    }
    function getId(){
        return $this -> id;
    }

    function setNome($nome){
        $this->nome = $nome;
    }
    function getNome(){
        return $this -> nome;
    }

    function setQuantidade($quantidade){
        $this->quantidade = $quantidade;
    }
    function getQuantidade(){
        return $this -> quantidade;
    }

    function setValor($valor){
        $this->valor = $valor;
    }
    function getValor(){
        return $this -> valor;
    }



}